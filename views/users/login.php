<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Money Tracking</h1>
            <br>
            <div class="account-wall">
                <img class="profile-img" src="" alt="">
                
                <form class="form-signin" action="<?php echo APP_URL."/users/login"; ?>" method="post">
                <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
                <br>
                <input name="password" type="password" class="form-control" placeholder="Password" required>
                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit" value="Entrar">
                    Sign in</button>
                </form>
            </div>
            
        </div>
    </div>
</div>

