<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Add user</h2></div>
<div class="panel-body">

<form action="<?php echo APP_URL."/users/add"; ?>" method="POST">
    <p>
        <label for="username">Username:</label>
        <input type="text" name="username">
    </p><br>
    <p>
        <label for="password">Password:</label>
        <input type="password" name="password">
    </p><br>
    <p>
    <div class="form-group">
        <label for="type_id">Types</label>
        <select class="form-control" name="type_id" id="type_id">
            <?php
            foreach ($types as $type):?>
            <option value="<?php echo $type["types"]["id"];?>">
                <?php echo $type ["types"] ["name"];?>
            </option>
            <?php endforeach;?>
        </select>
        </div>
    </p><br>
    <p>
       <button type="submit" class="btn btn-primary">Add</button>
    </p>

</form>
</div>
<div class="panel-footer">Money Tracking</div>
</div>
