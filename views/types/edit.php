<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Edit type user</h2></div>
<div class="panel-body">

<form action="<?php echo APP_URL."/types/edit"; ?>" method="POST">
	<input class="form-control" type="hidden" name="id" value="<?php echo $type["id"]; ?>">
    <p>
        <label for="name">Name:</label>
        <input class="form-control" type="text" name="name" value="<?php echo $type["name"]; ?>">
    </p>
    <p>
       <button type="submit" class="btn btn-primary">Edit</button>
    </p>

</form>
</div>
<div class="panel-footer">Money Tracking</div>
</div>