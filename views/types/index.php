<div class="container">
<div class="row">
<div class="col-xs-12">
<a href="types/add">Add Types Users</a>
<h2>Type user</h2>
<?php if(!empty($types)): ?>

<div class="table-responsive">
<table class="table">
	<tr>
		<th>Id</th>
		<th>Name</th>
				<th>Options</th>
	</tr>
	<?php
		foreach ($types as $type): 
	?>
	<tr>
		<td><?php echo $type["types"]["id"]; ?></td>
		<td><?php echo $type["types"]["name"]; ?></td>
		
		<td>
            <?php
            echo $this->Html->link("Edit", array(
                "controller"=>"types",
                "method"=>"edit",
                "arg"=>$type["types"]["id"]
));?> |
            <?php
           echo $this->Html->link("Delete", array(
                "controller"=>"types",
                "method"=>"delete",
                "arg"=>$type["types"]["id"]
            ));?>
        </td>
			<!--<a href="<?php echo APP_URL."/types/edit/".$type["types"]["id"]; ?>">Edit</a>-->
			<!--<a href="<?php echo APP_URL."/types/delete/".$type["types"]["id"]; ?>">Delete</a>-->
		</td>
	</tr>
	<?php 
		endforeach; 
	?>
</table>
</div>
</div>
<?php endif; ?>
</div>
</div>
</div>