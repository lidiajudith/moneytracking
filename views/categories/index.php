<div class="container">
<div class="row">
<div class="col-xs-12">


<h2>List categories</h2>
<?php if(!empty($categories)): ?>
<a href="categories/add"><span class="glyphicon glyphicon-user">Add Categories</a></span>
<div class="table-responsive">
<table class="table">
	<tr>
		<th>Id</th>
		<th>Name</th>
				<th>Options</th>
	</tr>
	<?php
		foreach ($categories as $category): 
	?>
	<tr>
		<td><?php echo $category["categories"]["id"]; ?></td>
		<td><?php echo $category["categories"]["name"]; ?></td>
		
		<td>
            <?php
            echo $this->Html->link("Edit", array(
                "controller"=>"categories",
                "method"=>"edit",
                "arg"=>$category["categories"]["id"]
));?> |
            <?php
           echo $this->Html->link("Delete", array(
                "controller"=>"categories",
                "method"=>"delete",
                "arg"=>$category["categories"]["id"]
            ));?>
        </td>
			<!--<a href="<?php echo APP_URL."/categories/edit/".$category["categories"]["id"]; ?>">Edit</a>-->
			<!--<a href="<?php echo APP_URL."/categories/delete/".$category["categories"]["id"]; ?>">Delete</a>-->
		</td>
	</tr>
	<?php 
		endforeach; 
	?>
</table>
</div>
</div>
<?php endif; ?>
</div>
</div>
</div>