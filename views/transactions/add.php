<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Add transaction</h2></div>
<div class="panel-body">


<form action="<?php echo APP_URL."/transactions/add"; ?>" method="POST">
    <p>
        <label for="operation">Operation:</label>
        <div class="form-group">
        <select class="form-control" name="operation" id="operation">
            <option value="egreso">Egreso</option>
            <option value="ingreso">Ingreso</option>
        </select>
        </div>
    </p><br>
    <p>  
        <label for="account_id">Account</label>
        <div class="form-group">
        <select class="form-control" name="account_id" id="account_id">
            <?php
            foreach ($accounts as $account):?>
            <option value="<?php echo $account["accounts"]["id"];?>">
                <?php echo $account ["accounts"] ["name"];?>
            </option>
            <?php endforeach;?>
        </select>
        </div>
    </p><br>
    <p>
        <label for="category_id">Category</label>
        <div class="form-group">
        <select class="form-control" name="category_id" id="category_id">
            <?php
            foreach ($categories as $category):?>
            <option value="<?php echo $category["categories"]["id"];?>">
                <?php echo $category ["categories"] ["name"];?>
            </option>
            <?php endforeach;?>
        </select>
        </div>
    </p><br>
    <p>
        <label for="description">Description:</label>
        <input class="form-control" type="text" name="description">
    </p><br>
    <p>
        <label for="date">Date:</label>
        <input class="form-control" type="date" name="date">
    </p><br>
    <p>
        <label for="amount">Amount:</label>
        <input class="form-control" type="text" name="amount">
    </p><br>
    <p>
        <button type="submit" class="btn btn-primary">Add</button>
    </p>

</form>
