<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Edit account</h2></div>
<div class="panel-body">


<form action="<?php echo APP_URL."/accounts/edit"; ?>" method="POST">
    <input type="hidden" name="id" value="<?php echo $account["id"]; ?>">
    <p>
    	<label for="user_id">Username:</label>
        <select class="form-control" name="user_id" id="user_id">
            <?php 
            foreach ($users as $user):
                if ($user["users"]["id"] == $account["user_id"]) {
            ?>
                <option selected value="<?php echo $user["users"]["id"]; ?>">
                    <?php echo $user["users"]["username"]; ?>
                </option>
            <?php 
                } else {
            ?>
                <option value="<?php echo $user["users"]["id"]; ?>">
                    <?php echo $user["users"]["username"]; ?>
                </option>
            <?php
                }
                endforeach;
            ?>
        </select>
    </p>
    <p>
        <label for="name">Name:</label>
        <input class="form-control" type="text" name="name" value="<?php echo $account["name"]; ?>">
    </p>
    <p>
         <button type="submit" class="btn btn-primary">Edit</button>
    </p>

</form>