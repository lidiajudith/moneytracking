<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Add account</h2></div>
<div class="panel-body">

<form action="<?php echo APP_URL."/accounts/add"; ?>" method="POST">
    <p>
        <label for="id">User</label>
        <select class="form-control" name="user_id" id="user_id">
            <?php
            foreach ($users as $user):?>
            <option value="<?php echo $user["users"]["id"];?>">
                <?php echo $user ["users"] ["username"];?>
            </option>
            <?php endforeach;?>
        </select>
    </p><br>
    <p>
        <label for="name">Name:</label>
        <input class="form-control" type="text" name="name">
    </p><br>
    
    <p>
         <button type="submit" class="btn btn-primary">Add</button>
    </p>

</form>
