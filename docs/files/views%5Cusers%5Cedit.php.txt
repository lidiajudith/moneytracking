<div class="container">
<div class="panel panel-info">
<div class="panel-heading"><h2>Edit user</h2></div>
<div class="panel-body">

<form action="<?php echo APP_URL."/users/edit"; ?>" method="POST">
	<input type="hidden" name="id" value="<?php echo $user["id"]; ?>">
    <p>
        <label for="username">Username:</label>
        <input class="form-control" type="text" name="username" value="<?php echo $user["username"]; ?>">
    </p>
    <p>
        <label for="newPassword">Password:</label>
        <input class="form-control" type="txt" name="newPassword">
    </p>
    <p>
    <div class="form-group">
         <label for="type_id">Types</label>
        <select class="form-control" name="type_id" id="type_id">
            <?php
            foreach ($types as $type):
                if($type["types"]["id"]
                   == $user["type_id"]) { ?>
            <option selected value="<?php echo $type["types"]["id"];?>">
            <?php echo $type ["types"] ["name"];?>
            </option>
                    
                <?php }else{?>
            <option value="<?php echo $type["types"]["id"];?>">
            <?php echo $type ["types"] ["name"];?>
            </option>
           <?php }?>
            
            <?php endforeach;?>
        </select>
        </div>
    </p>
    <p>
        <button type="submit" class="btn btn-primary">Edit</button>
    </p>

</form>
</div>
<div class="panel-footer">Money Tracking</div>
</div>
