
<?php
/**
 * [__autoload Archivo para cargar las clases]
 * @param  [String] $className [recibe nombre de la clase]
 * @return [type]            []
 */
function __autoload($className){
    require_once(ROOT."libs".DS.$className.".PHP");
}