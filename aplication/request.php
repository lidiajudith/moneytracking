<?php

cLass request
{
    private $_controller;
    private $_method;
    private $_args;

    /**
     * [__construct contructor de la clase]
     */
    public function __construct(){
      if(isset($_GET['url'])){
    		$url = filter_input(INPUT_GET, 'url' , FILTER_SANITIZE_URL);
    		$url = explode("/", $url);
    		$url = array_filter($url);
    		$this->_controller = strtolower(array_shift($url));
        if (count($url)>0) {
          $this->_method = strtolower(array_shift($url));
        }else {
          $this->_method = "index";
        }
        $this->_args = $url;

    	}

    	if (!isset($this->_controller)) {
    		$this->_controller = DEFAULT_CONTROLLER;
    	}

    	if (!isset($this->_method)) {
    		$this->_method = "index";
    	}

    	if (empty($this->_args)) {
    		$this->_args = array(
    			0=>NULL
    		  );
        }
      }
      /**
       * [getController obtiene el controlador]
       * @return [String] [retorna controlador]
       */
      public function getController(){
          return $this->_controller;
      }
      /**
       * [getMethod Obtiene método]
       * @return [String] [returno el mismo método]
       */
      public function getMethod(){
          return $this->_method;
      }

      /**
       * [getArgs obtiene argumentos]
       * @return [String] [retorna el argumento del método]
       */
      public function getArgs(){
          return $this->_args;
      }
}
