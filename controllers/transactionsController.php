<?php
/**
* Controlador para las transacciones
* Pemite la gestión y control de las transacciones
* @author Lidia Judith Poot Chi <jedialid@gmail.com>
*
* clase para las transacciones de usuarios
*/

cLass transactionsController extends AppsController
{

  public function __construct(){
    parent::__construct();
  }

  /**
   * [index Función que permite enlistar las transacciones]
   * @return [array] [conjunto de los montos de las transacciones]
   */
  public function index(){
    $conditions = array("conditions"=>"transactions.account_id=accounts.id and transactions.category_id=categories.id");

    $columnaSuma = array(
        "columna" =>"transactions.amount"
      );

    $this->set("transactions", $this->transactions->find("transactions, accounts, categories",  "all", $conditions));
    $this->set("transactionCount", $this->transactions->find("transactions", "count"));
    $transactionsSuma = $this->transactions->find("transactions", "suma", $columnaSuma);
    $this->set(compact("transactions", "transactionCount", "transactionsSuma"));
  }

  /**
   * [add Función que permite agregar transacciones]
   */
  public function add(){
   // if ($_SESSION["type_name"]=="Administradores") {
      if ($_POST) {
      if ($_POST["operation"] == 'egreso'){
        $_POST["amount"] = $_POST["amount"]*(-1);
      }
      if ($this->transactions->save("transactions", $_POST)) {
        $this->redirect(array("controller"=>"transactions"));
      }else{
        $this->redirect(array("controller"=>"transactions", "method"=>"add"));
      }
    }
      
      $this->set("accounts", $this->transactions->find("accounts"));
      $this->set("categories", $this->transactions->find("categories"));
      $this->_view->setView("add");
   /* }else{
      $this->redirect(array("controller"=>"transactions"));
    }*/
  }  

  /**
   * [edit Funcion que permite editar todas las transacciones]
   * @param  [Integer] $id [Id de las transacciones]
   * @return [String]     [Muestra las transacciones de la cuenta a editar]
   */
  public function edit($id){
      if ($id) {
        $options = array(
          "conditions" => "id=".$id
        );
        $transaction = $this->transactions->find("transactions","first", $options);
        $this->set("transaction", $transaction);
          $this->set("accounts", $this->transactions->find("accounts"));
          $this->set("categories", $this->transactions->find("categories"));
      }

      if($_POST){
        if ($_POST["operation"] == 'egreso'){
          $_POST["amount"] = $_POST["amount"]*(-1);
        }
        if ($this->transactions->update("transactions", $_POST)){
          $this->redirect(
            array(
              "controller"=>"transactions"
            ));
        }else{
          $this->redirect(
            array(
              "controller"=>"transactions",
              "method"=>"edit/".$_POST["id"]
            )
          );
        }
      }
  }

    /**
     * [delete Función que permite eliminar las transacciones]
     * @param  [Integer] $id [Id de la transacción a eliminar]
     * @return [Boolean]     [Elimina la cuenta]
     */
    public function delete($id){
        $conditions = "id=".$id;
      if(  $this->transactions->delete("transactions",$conditions)){
          $this->redirect(array("controller"=>"transactions"));
      }else{
          $this->redirect(array("controller"=>"transactions","method"=>"add"));
      }
    }
}
