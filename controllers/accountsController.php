<?php

/**
* Controlador para la cuenta de los usuarios
* Pemite la gestión y control de las cuentas de usuario
* @author Lidia Judith Poot Chi <jedialid@gmail.com>
*
* clase para la cuenta de usuarios
*/
cLass accountsController extends AppsController
{

  public function __construct(){
    parent::__construct();
  }

  /**
   * [Index es la página principal de la aplicación]
   * @return [String] [contiene los datos de los usuarios a ingresar]
   */
  public function index(){
    $conditions = array("conditions"=>"accounts.user_id=users.id");
    $this->set("accounts", $this->accounts->find("accounts, users",  "all", $conditions));
    $this->set("accountsCount", $this->accounts->find("accounts", "count"));
  }

  /**
   * [Funcion agregar, agrega cuenta del usuario]
   *
   * Verifica si es un administrador, si no es un administrador, No le permite crear usuario, de lo contrario sí.
   */
  public function add(){
    if ($_SESSION["type_name"]=="Administradores") {
      if ($_POST) {
      if ($this->accounts->save("accounts", $_POST)) {
        $this->redirect(array("controller"=>"accounts"));
      }else{
        $this->redirect(array("controller"=>"transactions", "method"=>"add"));
      }
    }
      
      $this->set("users", $this->accounts->find("users"));
      $this->_view->setView("add");
    }else{
      $this->redirect(array("controller"=>"accounts"));
    }
  }   

/**
 * [Función editar, edita cuenta del usuario]
 * @param  [integer] $id [id de la cuenta del usuario]
 * @return [array]     [devuelve si la cuenta esta registrada o no]
 */
  public function edit($id){
      if ($id) {
        $options = array(
          "conditions" => "id=".$id
        );
        $account = $this->accounts->find("accounts","first", $options);
        $this->set("account", $account);
        $this->set("users", $this->accounts->find("users"));
      }

      if($_POST){
        if ($this->accounts->update("accounts", $_POST)){
          $this->redirect(
            array(
              "controller"=>"accounts"
            ));
        }else{
          $this->redirect(
            array(
              "controller"=>"accounts",
              "method"=>"edit/".$_POST["id"]
            )
          );
        }
      }
  }

    /**
     * [Función delete, elimina la cuenta del usuario]
     * @param  [integer] $id [id de la cuenta a eliminar]
     * @return [array]     [si la cuenta está registrada se elimina]
     */
    public function delete($id){
        $conditions = "id=".$id;
      if(  $this->accounts->delete("accounts",$conditions)){
          $this->redirect(array("controller"=>"accounts"));
      }else{
          $this->redirect(array("controller"=>"accounts","method"=>"add"));
      }
    }
    
    /**
     * [Función Login, autoriza el inicio de sesión a los usuarios]
     * @return [array] [Devuelve si el usuario es válido o no]
     */
    public function login(){
    $this->_view->setLayout("login");
        
        if($_POST){
            
        $pass= new Password();
        $auth= new Authorization();
        $filter= new Validations();
        
        $username = $filter->sanitizeText($_POST["username"]);
        $password = $filter->sanitizeText($_POST["password"]);
        
        $options = array(
          "field" => 
            "accounts.id as user_id,
            accounts.password as password,
            accounts.username as username, 
            users.name as type_name",
            "conditions"=>"username='$username' and accounts.user_id=users.id"
          );
        $user= $this->accounts->find("accounts, users", "first", $options);
        if($pass->isValid($password, $user["password"])){
            $auth->login($user);
            $this->redirect(array("controller"=>"pages"));
        }else{
            echo "Usuario no valido";
        }
        }
    }

    /**
     * [Función logout, ciera la sesión del usuario]
     * @return [array] [devuelve el usuario que está registrado y cierra la sesion]
     */
    public function logout(){
      $auth = new Authorization();
      $auth->logout();
      $this->_view->render("login");
    }
}
