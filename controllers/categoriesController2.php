<?php
/**
 * Controlador categorías
 * @author Lidia Judith Poot Chi <jedialid@gmail.com>
 */
class CategoriesController extends AppController
{
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Metodo Index
	 * Metodo que permite hacer el listado de categorias de las transacciones
	 * @return [String] [ontiene el conjunto de categorias]
	 * 
	 */
	public function index(){
		$this->set("categories", $this->Categories->find("Categories"));
	}
}
?>